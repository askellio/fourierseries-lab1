name := "lab1"

version := "1.0"

scalaVersion := "2.12.1"


libraryDependencies += "com.github.wookietreiber" %%
  "scala-chart" %
  "latest.integration"

scalacOptions ++= Seq("-unchecked", "-deprecation")