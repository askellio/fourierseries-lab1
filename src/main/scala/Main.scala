/**
  * Created by askellio on 10/23/17.
  */

package me.eax.scalachart

import scalax.chart._
import scalax.chart.Chart._
import java.io._
import Math._

import org.jfree.chart.plot.Plot
import org.jfree.data.xy.XYSeries

import scalax.chart.module.XYChartFactories.XYLineChart

object Main extends App {


  var left = -2.0
  var right = 2.0
  val medium = 0.0

  def func (value: Double): Double = {
    if (value <= medium)
      4.0
    else
      2.0*value/3.0 - 1.0
  }

  def funcS (value: Double, n: Int): Double = {
    var sum: Double = 11/6
    for (i <- 1 to n) {
      sum = sum + (cos(PI*i*value/2))*
        (
          (8*sin(2*pow(PI, 2)*pow(i,2)/4)/(PI*i)) +
            (2*sin(PI*i))/(3*PI*i) +
            (4*(cos(pow(PI, 2)*pow(i, 2)*2/4)-1))/(3*PI*i*PI*i/2)
          )/2
    }

    sum
  }

  val defaultChartSize = (512, 384)


  val series = new XYSeries("f(x) = func(x)")
  val series1 = new XYSeries("f(x) = s1(x)")
  val series2 = new XYSeries("f(x) = s2(x)")
  val series3 = new XYSeries("f(x) = s3(x)")
  val chart = XYLineChart(series)
  val chart1 = XYLineChart(series1)
  val chart2 = XYLineChart(series2)
  val chart3 = XYLineChart(series3)

  chart.show()
  chart1.show()
  chart2.show()
  chart3.show()
  for (x <- left to (right, 0.1)) {
    swing.Swing onEDT {
      series.add(x,func(x))
      series1.add(x, funcS(x, 1))
      series2.add(x, funcS(x, 2))
      series3.add(x, funcS(x, 3))

    }
  }


}
